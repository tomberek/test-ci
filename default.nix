{ sources ? import ./nix/sources.nix }:
let
  pkgs = import sources.nixpkgs{
    system = "x86_64-linux";
  };
  lib = pkgs.lib;
  inherit (import sources.gitignore { inherit (pkgs) lib; }) gitignoreSource;
in
with pkgs; with lib; with builtins;
hello 
